#!/bin/bash
interface="enp34s0"
local_network=$(ip -o -f inet addr show | awk '$2=="enp34s0" {print $4}')
nmap -oG report.txt $local_network
