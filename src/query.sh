#!/bin/bash

# Option status
show_help=0
show_fields=0
query_args=""

# Provides options to query
while [[ $# -gt 0 ]]; do
    arg="${1#*=}"
    case "$1" in
        (-h|--help)         show_help=1 ;;
        (-f|--fields)       show_fields=1 ;;
        (--query)           shift; query_args=$1 ;;
        (--query=*)         query_args=$arg ;;

        (-*)                echo -e "Unrecognised option '$1'!" ;;
        (*)                 break
    esac
    shift
done

# Gives a description of the program
if [ $show_help == 1 ]; then
    echo -e ""
    echo -e "  Description:         Queries a small database of hardware information"
    echo -e "  Usage:               $ inventory [options]"
    echo -e ""
    echo -e "  Options:"
    echo -e "  --help,-h:           Provides information about the program"
    echo -e "  --fields,-f:         Provides a list of available fields to be queried"
    echo -e "  --query=<fields>:    Provides the data of specified (comma-separated) fields"
    echo -e ""

# Shows the available fields
elif [ $show_fields == 1 ]; then
    source ./inventory.sh
    get_fields

# Shows the data of specified fields
elif [ "$query_args" != "" ]; then
    source ./inventory.sh
    query_record $query_args
fi