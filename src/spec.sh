#!/bin/bash

# Returns all the fields
get_fields() {
    local fields=(
        "hostname","os","kernel","model",
        "cpu","num_cores","real_cores","memory_total","memory_available","memory_free",
        "gpu_type","gpu_memory_total","gpu_memory_available","gpu_memory_free",
        "ethernet_card","wifi_card","serial_number",
        "users","update_time","location","notes"
    )
    echo -e "${fields[@]}" | tr -d " "
}

# Returns a single value based on the input field
get_values() {

    # Queries for all the values
    declare -A data_arr=(
        ["hostname"]=$(echo -n "$(hostname)")
        ["os"]=$(cat /etc/lsb-release | grep DESCRIPTION | cut -f2 -d= | sed 's/"//g')
        ["kernel"]=$(uname -r)
        ["model"]="model" # $(sudo dmidecode -s system-product-name)
        ["cpu"]=$(lscpu | grep "Model name" | cut -d ' ' -f3- | awk '{$1=$1};1')
        ["num_cores"]=$(lscpu | grep "CPU(s)" | awk '$1=="CPU(s):" {print $2}')
        ["real_cores"]=$(lscpu | grep "Core(s) per socket" | awk '{print $4}')
        ["memory_total"]=$(cat /proc/meminfo | grep "MemTotal" | cut -d ' ' -f2- | awk '{$1=$1};1')
        ["memory_available"]=$(cat /proc/meminfo | grep "MemAvailable" | cut -d ' ' -f2- | awk '{$1=$1};1')
        ["memory_free"]=$(cat /proc/meminfo | grep "MemFree" | cut -d ' ' -f2- | awk '{$1=$1};1')
        ["gpu_type"]=$(cat /proc/driver/nvidia/gpus/0000\:26\:00.0/information | grep "Model" | sed "s/^[^ ]* //" | awk '{$1=$1};1')
        ["gpu_memory_total"]=$(glxinfo | egrep -i 'device|memory' | grep "Dedicated video memory" | awk '{$1=$1};1' | cut -d ' ' -f4-) # mesa-utils
        ["gpu_memory_available"]=$(glxinfo | egrep -i 'device|memory' | grep "Total available memory" | awk '{$1=$1};1' | cut -d ' ' -f4-) # mesa-utils
        ["gpu_memory_free"]=$(glxinfo | egrep -i 'device|memory' | grep "Currently available" | awk '{$1=$1};1' | cut -d ' ' -f6-) # mesa-utils
        ["ethernet_card"]=$(lspci | egrep -i 'ethernet' | cut -d ' ' -f4- | awk '{$1=$1};1')
        ["wifi_card"]=$(lspci | egrep -i 'wi-fi' | cut -d ' ' -f4- | awk '{$1=$1};1')
        ["serial_number"]="serial_number" # $(sudo dmidecode -s system-serial-number)
        ["users"]="$(ls /home | tr "\n" : | sed 's/.$//')"
        ["update_time"]="$(date '+%Y%m%d%H%M%S')"
    )

    # Convert input to array
    local fields=$(echo $1 | tr "," " ")
    read -a fields <<< "$fields"

    # Get values from data array and return
    local field_values=""
    for field in "${fields[@]}"; do
        field_values+="${data_arr["$field"]},"
    done
    echo ${field_values::-1}
}

while getopts ":fv: --long fields" option; do
   case $option in
     f) # gets fields
        get_fields;;
     v) # gets values
        get_values $OPTARG;;
    \?) # Invalid option
        echo "Error: Invalid option"
        exit;;
   esac
done
